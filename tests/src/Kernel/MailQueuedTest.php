<?php

namespace Drupal\Tests\welcome_mail\Kernel;

use Drupal\Core\Test\AssertMailTrait;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the JavaScript functionality of the Welcome mail module.
 *
 * @group welcome_mail
 */
class MailQueuedTest extends KernelTestBase {

  use AssertMailTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'welcome_mail',
    'user',
    'system',
    'filter',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    $this->installEntitySchema('user');
    if (version_compare(\Drupal::VERSION, '10.2', '<')) {
      $this->installSchema('system', 'sequences');
    }
    $this->installSchema('user', ['users_data']);
    $this->installConfig(['filter', 'welcome_mail']);
  }

  /**
   * Tests that a user is queued.
   */
  public function testQueue() {
    // Create a user.
    /** @var \Drupal\user\UserStorageInterface $user_storage */
    $user_storage = $this->container->get('entity_type.manager')->getStorage('user');
    $user = $user_storage->create([
      'name' => 'testuser1',
      'mail' => 'testuser1@example.com',
    ]);
    $user->save();
    /** @var \Drupal\Core\Queue\QueueFactory $queue_factory */
    $queue_factory = $this->container->get('queue');
    $queue = $queue_factory->get(WELCOME_MAIL_QUEUE_NAME);
    // Should not trigger a queue item, since we have not enabled anything yet.
    self::assertEquals(0, $queue->numberOfItems());
    // Enable the welcome mail.
    $this->config('welcome_mail.settings')
      ->set('enabled', TRUE)
      ->save();
    // Create a user.
    /** @var \Drupal\user\UserInterface $user */
    $user = $user_storage->create([
      'name' => 'testuser2',
      'mail' => 'testuser2@example.com',
    ]);
    // Set the user to a couple days old, so we know they will get the mail.
    $user->set('created', time() - (3600 * 48));
    $user->save();
    $queue = $queue_factory->get(WELCOME_MAIL_QUEUE_NAME);
    // Should now hold an item.
    $this->assertEquals(1, $queue->numberOfItems());
    // Try to run the queue.
    /** @var \Drupal\Core\Queue\QueueWorkerManagerInterface $queue_runner */
    $queue_runner = $this->container->get('plugin.manager.queue_worker');
    // Process all of them.
    /** @var \Drupal\welcome_mail\Plugin\QueueWorker\WelcomeMailQueue $wc_queue */
    $wc_queue = $queue_runner->createInstance(WELCOME_MAIL_QUEUE_NAME);
    $item = $queue->claimItem();
    if (empty($item->data)) {
      throw new \Exception('No data found when trying to claim data');
    }
    $wc_queue->processItem($item->data);
    $mails = $this->getMails([
      'id' => 'welcome_mail_welcome',
      'to' => $user->getEmail(),
      'key' => 'welcome',
    ]);
    self::assertEquals(count($mails), 1, 'Number of emails was as expected');
  }

  /**
   * Test that deleted users does not fudge up our queues.
   */
  public function testDeletedUserQueued() {
    // Enable the welcome mail.
    $this->config('welcome_mail.settings')
      ->set('enabled', TRUE)
      ->save();
    // Create a user.
    /** @var \Drupal\user\UserStorageInterface $user_storage */
    $user_storage = $this->container->get('entity_type.manager')->getStorage('user');
    $user1 = $user_storage->create([
      'name' => 'testuser1',
      'mail' => 'testuser1@example.com',
    ]);
    $user1->save();
    // Create another user.
    $user2 = $user_storage->create([
      'name' => 'testuser2',
      'mail' => 'testuser2@example.com',
    ]);
    $user2->save();
    // And create another one.
    $user3 = $user_storage->create([
      'name' => 'testuser3',
      'mail' => 'testuser3@example.com',
    ]);
    $user3->save();
    // Delete the first two users.
    $user1->delete();
    $user2->delete();
    /** @var \Drupal\Core\Queue\QueueFactory $queue_factory */
    $queue_factory = $this->container->get('queue');
    $queue = $queue_factory->get(WELCOME_MAIL_QUEUE_NAME);
    // Should now hold 3 items.
    $this->assertEquals(3, $queue->numberOfItems());
    /** @var \Drupal\Core\Queue\QueueWorkerManager $queue_runner */
    $queue_runner = $this->container->get('plugin.manager.queue_worker');
    // Process all of them.
    $wc_queue = $queue_runner->createInstance(WELCOME_MAIL_QUEUE_NAME);
    $regex = '/^There has not passed enough time for user uid/';
    $this->expectExceptionMessageMatches($regex);
    while ($queue->numberOfItems()) {
      $item = $queue->claimItem();
      if (!$item || empty($item->data)) {
        break;
      }
      $wc_queue->processItem($item->data);
    }
    $this->assertEquals(1, $queue->numberOfItems());
  }

}
