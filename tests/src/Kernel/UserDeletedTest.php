<?php

namespace Drupal\Tests\welcome_mail\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Test that things work when a user in the queue was deleted.
 *
 * @group welcome_mail
 */
class UserDeletedTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * User.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['welcome_mail', 'user', 'system'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    if (version_compare(\Drupal::VERSION, '10.2', '<')) {
      $this->installSchema('system', 'sequences');
    }
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('user');
    $this->user = $this->createUser([]);
  }

  /**
   * Test that the fact that we delete a user does not crash the entire queue.
   */
  public function testHandleDeleted() {
    $queue = $this->container->get('queue')->get(WELCOME_MAIL_QUEUE_NAME);
    $queue->createItem($this->user->id());
    $this->assertEquals(1, $queue->numberOfItems());
    /** @var \Drupal\Core\Queue\QueueWorkerManager $queue_manager */
    $queue_manager = $this->container->get('plugin.manager.queue_worker');
    $worker = $queue_manager->createInstance(WELCOME_MAIL_QUEUE_NAME);
    $item = $queue->claimItem();
    $this->user->delete();
    // Now, running this should not throw an exception.
    if (empty($item->data)) {
      throw new \Exception('No data found when trying to claim data');
    }
    $worker->processItem($item->data);
  }

}
