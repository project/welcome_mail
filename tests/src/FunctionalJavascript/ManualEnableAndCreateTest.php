<?php

namespace Drupal\Tests\welcome_mail\FunctionalJavascript;

use Drupal\Core\Url;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Test welcome mail config form and enable option.
 *
 * @group welcome_mail
 */
class ManualEnableAndCreateTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'welcome_mail',
    'filter',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    $this->adminUser = $this->createUser([
      'administer welcome_mail configuration',
    ]);
  }

  /**
   * Test that we can go to the settings page and enable the functionality.
   *
   * @see https://www.drupal.org/project/welcome_mail/issues/3168913
   */
  public function testMailQueuedAfterEnabled() {
    /** @var \Drupal\Core\Queue\QueueFactory $queue_factory */
    $queue_factory = $this->container->get('queue');
    $queue = $queue_factory->get(WELCOME_MAIL_QUEUE_NAME);
    $this->assertEquals(0, $queue->numberOfItems());
    $this->drupalLogin($this->adminUser);
    $url = Url::fromRoute('welcome_mail.settings_form');
    $this->drupalGet($url);
    // We check this to make sure the actual checkbox on that page does what it
    // should, regardless of if it has a typo.
    $this->click('input[type="checkbox"]');
    $this->click('input[type="submit"]');
    // Create a user.
    /** @var \Drupal\user\UserStorageInterface $user_storage */
    $user_storage = $this->container->get('entity_type.manager')->getStorage('user');
    /** @var \Drupal\user\UserInterface $user */
    $user = $user_storage->create([
      'name' => 'testuser2',
      'mail' => 'testuser2@example.com',
    ]);
    $user->save();
    $queue = $queue_factory->get(WELCOME_MAIL_QUEUE_NAME);
    $this->assertEquals(1, $queue->numberOfItems());
  }

}
