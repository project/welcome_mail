# Welcome mail

Welcome Mail module allows you to send a welcome mail after
x amount of time to new users.

Welcome Mail is often used if you are running a service and
want to check in on how people are doing after a while, and
if they have any questions or improvements.

Welcome Mail module is what sends out the welcome mails at
Violinist.io, which is why it was developed initially.

- For a full description of the module, visit the [project page](https://www.drupal.org/project/welcome_mail).

- Submit bug reports and feature suggestions, or track changes in the [issue queue](https://www.drupal.org/project/issues/welcome_mail?categories=All).


## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Recommended modules

No recommended modules.


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Configure user permissions in Administration » People » Permissions:
    1. Use the administration pages and help (System module).

    The top-level administration categories require this permission
    to be accessible.The administration menu will be empty unless this
    permission is granted.

    2. Access administration menu.

    Users in roles with the "Access administration menu" permission will
    see the administration menu at the top of each page.


## Maintainers

Current maintainers:
- Eirik Morland - [eiriksm](https://www.drupal.org/u/eiriksm)
